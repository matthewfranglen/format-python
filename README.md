Format Python
=============

This script allows you to invoke yapf and isort easily.

Installation
------------
If you have [antigen](https://github.com/zsh-users/antigen) then just run the following:

```bash
antigen-bundle https://gitlab.com/matthewfranglen/format-python
```

Otherwise just clone this repo and add the `format-python` command to the $PATH.

Usage
-----

Call this script with the file(s) to format.
